﻿using LorinthsLair_RPGUtilsML.Model;

namespace LorinthsLair.RPGUtils
{
    public class NameGenderSelector
    {
        /// <summary>
        /// Predict gender based on name
        /// </summary>
        /// <param name="name">
        /// Name to predict gender on
        /// </param>
        /// <returns>
        /// LorinthsLair.RPGUtilML.Model.ModelOutput
        /// Prediction - 1 = Female, 0 = Male
        /// Score - 0 index value = Female probability, 1st index value = Male probability
        /// </returns>
        public static ModelOutput PredictGender(string name)
        {
            ModelInput input = new ModelInput()
            {
                Name = name
            };

            return ConsumeModel.Predict(input);
        }
    }
}
