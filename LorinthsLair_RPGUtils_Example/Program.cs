﻿using System;
using LorinthsLair.RPGUtils;

namespace LorinthsLair.RPGUtils.Example
{
    internal class Program
    {
        private static void Main()
        {
            string msg = "Name Generator:\n\n";

            for (byte i = 1; i <= 50; i++)
            {
                string name = NameGeneration.GenerateName(6);
                var genderPrediction = NameGenderSelector.PredictGender(name);
                msg += $"{i}: {name} {(genderPrediction.Prediction == "1" ? "F" : "M")} {(NameGeneration.ValidateName(name) ? "V" : "")} ";
                msg += $"{String.Join(',', genderPrediction.Score)}\n";
            }

            Console.WriteLine(msg);

            for (int i = 0; i < 5; i++)
            {
                var npc = NPCGenerator.GenerateNPC();

                Console.WriteLine($@"
Name: {npc.Name}
Gender: {npc.Gender}
Race: {npc.Race}
Age: {npc.Age}
Height: {npc.Height}
Weight: {npc.Weight}
Appearance: {npc.Appearance}
Moral alignment: {npc.MoralAlignment}
Occupation: {npc.Occupation}
Flaws: {npc.Flaws}
");
            }
        }
    }
}
