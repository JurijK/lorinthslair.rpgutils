﻿using System;
using System.Collections.Generic;

namespace LorinthsLair.RPGUtils
{
    public class NameGeneration
    {
        private static readonly Random _random = new Random(DateTime.Now.Millisecond);
        private static readonly List<string> _vowels = new List<string>() 
        { 
            "a", "e", "i", "o", "u"
        };
        private static readonly List<string> _consonants = new List<string>() 
        {
            "b", "c", "d", "f", "g", "h", "j", "k", "m", "n", "l", "p", "r", "s", "t", "w", "y", "z" 
        };
        private static byte consonantsInRow = 0;
        private static readonly List<string> _validationSyllables = new List<string>()
        {
            "ac", "act", "ad", "af", "ag", "age", "air", "al", "als", "am",
            "an", "ap", "ar", "as", "at", "ate", "au", "ba", "bat", "be",
            "ber", "bers", "bet", "bi", "ble", "bles", "bod", "bor", "bout", "but",
            "by", "ca", "cal", "can", "cap", "car", "cat", "cate", "cen", "cent",
            "char", "ci", "cial", "cir", "cit", "cle", "co", "col", "com", "come",
            "con", "cor", "coun", "cov", "cu", "cul", "cus", "cy", "da", "dan",
            "day", "de", "den", "der", "ders", "di", "dif", "dis", "dle", "dy",
            "east", "ed", "ef", "el", "en", "ence", "end", "ent", "er", "ered",
            "ern", "ers", "es", "est", "et", "ev", "eve", "ex", "fa", "fac",
            "fect", "fer", "fi", "fin", "fish", "fix", "fol", "for", "fore", "form",
            "ful", "gan", "gen", "ger", "gi", "gle", "go", "grand", "great", "hap",
            "har", "head", "heav", "high", "ho", "hunt", "ic", "ies", "il", "im",
            "in", "ing", "ings", "ion", "is", "ish", "it", "its", "jo", "ket",
            "la", "land", "lands", "lar", "lat", "lead", "lec", "lect", "lent", "less",
            "let", "li", "light", "lin", "lo", "long", "low", "lu", "ly", "ma",
            "mag", "main", "mal", "man", "mar", "mat", "me", "meas", "mem", "men",
            "ment", "ments", "mer", "mi", "mil", "min", "mis", "mo", "mon", "moth",
            "mu", "mul", "my", "n't", "na", "nal", "near", "nel", "ner", "ness",
            "net", "nev", "new", "ni", "ning", "nit", "no", "nore", "nu", "num",
            "ny", "ob", "oc", "of", "on", "one", "op", "or", "oth", "ous",
            "out", "pa", "par", "prac", "parc", "pe", "pen", "peo", "per", "pi",
            "pic", "play", "ple", "ples", "ply", "po", "point", "por", "port", "pos",
            "pre", "pres", "press", "pro", "ra", "ral", "re", "read", "rec", "rect",
            "rep", "ri", "ried", "ro", "round", "row", "ry", "sa", "sand", "sat",
            "sent", "se", "sec", "self", "sen", "ser", "set", "ship", "si", "side",
            "sim", "sion", "sions", "sis", "so", "some", "son", "sons", "south", "stand",
            "stud", "su", "sub", "sug", "sun", "sup", "sur", "ta", "tain", "tal",
            "te", "ted", "tel", "tem", "ten", "tence", "tend", "ter", "ters", "the",
            "ther", "ti", "tic", "ties", "tin", "ting", "tion", "tions", "tive", "tle",
            "to", "tom", "ton", "tor", "tors", "tra", "tract", "tray", "tri", "tro",
            "try", "tu", "ture", "tures", "tween", "ty", "uer", "um", "un", "up",
            "ure", "us", "va", "val", "var", "vel", "ven", "ver", "vi", "vid",
            "vis", "wa", "ward", "way", "west", "where", "wil", "win", "won", "work", "writ"
        };

        /// <summary>
        /// Generate fantasy name.
        /// </summary>
        /// <param name="length">
        /// Length of name
        /// </param>
        /// <returns>
        /// Generated name
        /// </returns>
        /// <remarks>
        /// Result is not verified by searching for most common syllables in English.
        /// To verify use NameGeneration.ValidateName(string name)
        /// </remarks>
        public static string GenerateName(byte length)
        {
            string name = "";

            if (_random.NextDouble() > 0.2)
            {
                name += _consonants[_random.Next(0, _consonants.Count)].ToUpper();
                consonantsInRow++;
            }
            else
            {
                name += _vowels[_random.Next(0, _vowels.Count)].ToUpper();
            }

            if (length >= 2)
            {
                if (consonantsInRow == 1)
                {
                    name += _vowels[_random.Next(0, _vowels.Count)];
                    consonantsInRow = 0;
                }
                else
                {
                    name += _consonants[_random.Next(0, _consonants.Count)];
                    consonantsInRow++;
                }
            }

            for (int i = 2; i < length; i++)
            {
                double vowelChance = consonantsInRow switch
                {
                    0 => 0.15,
                    1 => 0.45,
                    2 => 0.7,
                    3 => 0.9,
                    _ => 1.0,
                };

                if (_random.NextDouble() > vowelChance)
                {
                    name += _consonants[_random.Next(0, _consonants.Count)];
                    consonantsInRow++;
                }
                else
                {
                    name += _vowels[_random.Next(0, _vowels.Count)];
                    consonantsInRow = 0;
                }
            }

            consonantsInRow = 0;

            return name;
        }

        /// <summary>
        /// Check if name contains one of most common syllables in English
        /// </summary>
        /// <param name="name">
        /// Name you want to validate
        /// </param>
        /// <returns>
        /// true - if name contains one or more syllables
        /// false - if not
        /// </returns>
        public static bool ValidateName(string name)
        {
            name = name.ToLower();

            foreach (string syllab in _validationSyllables)
            {
                if (name.Contains(syllab))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
