﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsLair.RPGUtils
{
    public static class RandomExtension
    {
        /// <summary>
        /// Generate random number within range based on Normal Distribution with given parameters
        /// </summary>
        /// <param name="random"></param>
        /// <param name="min">
        /// Minimum value
        /// </param>
        /// <param name="max">
        /// Maximum value
        /// </param>
        /// <param name="avg">
        /// Averange value (mean, mode, median)
        /// </param>
        /// <param name="standardDeviation">
        /// Standard deviation
        /// </param>
        /// <returns>
        /// Random number within range based on Normal Distribution
        /// </returns>
        /// <remarks>
        /// Due to unknown bug tests show that empirical standard deviation is different than given standard deviation as parameter.
        /// </remarks>
        public static double NextDoubleNaturalDist(this Random random, double min, double max, double avg, double standardDeviation)
        {
            double result;
            double u1;
            double u2;

            do
            {
                u1 = random.NextDouble();
                u2 = random.NextDouble();
                result = Math.Sqrt((-2) * Math.Log(u1)) * Math.Cos(2 * Math.PI * u2) * standardDeviation + avg;
            } while (result < min || result > max);

            return result;
        }

        /// <summary>
        /// Generate random number within range
        /// </summary>
        /// <param name="random"></param>
        /// <param name="min">
        /// Minimum value
        /// </param>
        /// <param name="max">
        /// Maximum value
        /// </param>
        /// <returns>
        /// Random number within range
        /// </returns>
        public static double NextDouble(this Random random, double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }
    }
}
