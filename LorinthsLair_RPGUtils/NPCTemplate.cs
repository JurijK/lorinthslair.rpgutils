﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LorinthsLair.RPGUtils
{
    public class NPCTemplate
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public string Race { get; set; }

        public int Age { get; set; }

        public string Height { get; set; }

        public string Weight { get; set; }

        public string Appearance { get; set;}

        public string MoralAlignment { get; set; }

        public string Occupation { get; set; }

        public string Flaws { get; set; }

        /* //Disabled
        public string Backstory { get; set; }

        public string Aspirations { get; set; }
        */
    }
}
