﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LorinthsLair.UnitConverter;

namespace LorinthsLair.RPGUtils
{
    public class NPCGenerator
    {
        private static readonly Random _random = new Random(DateTime.Now.Millisecond);
        private static readonly List<string> _races = new List<string>()
        {
            "Human", "Dwarf", "Elf", "Orc"
        };
        private static readonly List<string> _moralAlignment = new List<string>()
        {
            "Lawful good", "Neutral good", "Chaotic good",
            "Lawful neutral", "True neutral", "Chaotic neutral",
            "Lawful evil", "Neutral evil", "Chaotic evil"
        };
        private static readonly List<string> _occupations = new List<string>()
        {
            "Slave", "Beggar", "Farmer", "Innkeeper", "Woodcutter", "Hunter", "Bard", "Barkeeper", "Solder",
            "Gravedigger", "Armorer", "Blacksmith", "Miner", "Sailor", "Wizard", "Fortune-teller", "Herbalist",
            "Doctor", "Alchemist", "Prostitute", "Courtesan", "Acolyte", "Baker", "Banker", "Bounty Hunter",
            "Butcher", "Candlemaker", "Carpenter", "Shoemaker", "Dancer", "Engineer", "Fisherman", "Guard",
            "Mercenary", "Merchant", "Musician", "Painter", "Scholar", "Scribe", "Sculptor", "Shopkeeper", "Servant",
            "Singer", "Slave Trader", "Tailor", "Weaponsmith", "Messenger", "Apothecary", "Astrologer", "Brewer",
            "Cartographer", "Cook", "Dyer", "Falconer", "Ferryman", "Fishmonger", "Furrier", "Groom", "Healer",
            "Miller", "Monk", "Squire", "Peddler", "Potter", "Priest", "Shipwright", "Shepherd", "Thatcher", "Vintner",
            "Weaver"
        };
        private static readonly List<string> _flaws = new List<string>()
        {
            "Gambling problem", "Alcoholism", "Drug addiction", "Smoking problem", "Sadism", "Masochism", "Egoist", "Naive",
            "Misogyny", "Misandry", //Hate women/men
            "Achluophobia", //Fear of darkness
            "Acrophobia", //Fear of heights
            "Agoraphobia", //Fear of public spaces or crowds
            "Arachnophobia", //Fear of spiders
            "Catoptrophobia", //Fear of mirrors
            "Chionophobia", //Fear of snow
            "Cynophobia", //Fear of dogs
            "Elurophobia", //Fear of cats
            "Entomophobia", //Fear of insects
            "Equinophobia", //Fear of horses
            "Heliophobia", //Fear of the sun
            "Hemophobia", //Fear of blood
            "Herpetophobia", //Fear of reptiles
            "Hydrophobia", //Fear of water
            "Ombrophobia", //Fear of rain
            "Ophidiophobia", //Fear of snakes
            "Ornithophobia", //Fear of birds
            "Pyrophobia", //Fear of fire
            "Selenophobia", //Fear of the moon
            "Laziness"
        };
        private static readonly List<string> _eyeColors = new List<string>()
        {
            "Amber", "Blue", "Brown", "Gray", "Green", "Hazel", "Red", "Violet", "Heterochromia Iridum"
        };
        private static readonly List<string> _hairColors = new List<string>()
        {
            "Brown", "Blonde", "Black", "Auburn", "Red", "White"
        };
        private static readonly List<string> _hairStyles = new List<string>() //add different hairstyles
        {
            "Long", "Medium", "Short", "Bald"
        };
        private static readonly List<string> _bodyParts = new List<string>()
        {
            "Face", "Forehead", "Left temple", "Right temple", "Left eyebrow", "Right eyebrow",
            "Left eye", "Right eye", "Left cheek", "Right cheek", "Nose", "Left ear", "Right ear",
            "Upper lip", "Bottom lip", "Throat", "Chin",
            "Left arm", "Right arm", "Left armpit", "Right armpit", "Left forearm", "Right forearm",
            "Left elbow", "Right elbow", "Left wrist", "Right wrist", "Left palm", "Right palm",
            "Left hand", "Right hand", "Left thumb", "Right thumb", "Left index finger", "Right index finger",
            "Left middle finger", "Right middle finger", "Left ring finger", "Right ring finger",
            "Left pinky", "Right pinky",
            "Left hip", "Right hip", "Left thigh", "Right thigh", "Left knee", "Right knee",
            "Left calf", "Right calf", "Left ankle", "Right ankle", "Left foot", "Right foot",
            "Left heel", "Right heel",
            "Left breast", "Right breast", "Stomach", "Back", "Left shoulder", "Right shoulder",
            "Left buttock", "Right buttock", "Groin"
        };
        private static readonly List<string> _scarLength = new List<string>()
        {
            "Long", "Medium", "Short", "Point", "Circular"
        };
        private static readonly List<string> _scarDeepness = new List<string>()
        {
            "Deep", "Shallow"
        };

        /// <summary>
        /// Generate NPC template
        /// </summary>
        /// <returns>
        /// Generated NPC
        /// </returns>
        public static NPCTemplate GenerateNPC()
        {
            double height = (int)_random.NextDoubleNaturalDist(100, 251, 178, 30);
            double weight = (int)_random.NextDoubleNaturalDist(35, 251, 85, 25);
            NPCTemplate npc = new NPCTemplate();

            npc.Name = NameGeneration.GenerateName((byte)_random.Next(4, 11));
            npc.Gender = NameGenderSelector.PredictGender(npc.Name).Prediction == "1" ? "Female" : "Male";
            npc.Race = _races.ElementAt(_random.Next(0, _races.Count));

            npc.Age = (int)_random.NextDoubleNaturalDist(1.0, 121.0, 35.0, 20.0);
            npc.Height = $"{height/100.0}m ({LengthConverter.ParseCentimetersToFeet(height.ToString(), 0)})";
            npc.Weight = $"{weight}kg ({WeightConverter.ParseKilogramsToPounds(weight.ToString(), 0)})";

            npc.Appearance = GenerateAppearance(npc.Gender);
            npc.MoralAlignment = _moralAlignment.ElementAt(_random.Next(0, _moralAlignment.Count));
            npc.Occupation = _occupations.ElementAt(_random.Next(0, _occupations.Count));
            npc.Flaws = GenerateFlaws();

            /* //Disabled
            npc.Backstory = "";
            npc.Aspirations = "";
            */

            return npc;
        }

        private static string GenerateAppearance(string gender)
        {
            string appearance = "";

            string eyesColor = _eyeColors.ElementAt(_random.Next(0, _eyeColors.Count)).ToLower();
            string hair = $"{_hairStyles.ElementAt(_random.Next(0, _hairStyles.Count))} {_hairColors.ElementAt(_random.Next(0, _hairColors.Count))}".ToLower();
            int numberOfScars = (int)_random.NextDoubleNaturalDist(0, 6, 2, 2);

            appearance += $"{(gender == "Female" ? "She" : "He")} has {eyesColor} eyes, {hair} hair";

            if (numberOfScars == 0)
            {
                appearance += ".";
                return appearance;
            }
            
            appearance += $" and {numberOfScars} {(numberOfScars == 1 ? "scar:" : "scars:")}";

            for (int i = 0; i < numberOfScars; i++)
            {
                appearance += $"\n{GenerateScar()}";
            }

            return appearance;
        }

        private static string GenerateScar()
        {
            string length = _scarLength.ElementAt(_random.Next(0, _scarLength.Count));
            string deepness = _scarDeepness.ElementAt(_random.Next(0, _scarDeepness.Count));
            string bodyPart = _bodyParts.ElementAt(_random.Next(0, _bodyParts.Count));

            string scar = $"{length} {deepness.ToLower()} on {bodyPart.ToLower()}";

            return scar;
        }

        private static string GenerateFlaws()
        {
            string flaws = "";

            int numberOfFlaws = (int)_random.NextDoubleNaturalDist(0, 7, 3, 2);

            if (numberOfFlaws == 0)
            {
                return "None";
            }

            for (int i = 0; i < numberOfFlaws; i++)
            {
                if (i == numberOfFlaws - 1)
                {
                    flaws += _flaws.ElementAt(_random.Next(0, _flaws.Count));
                }
                else
                {
                    flaws += $"{_flaws.ElementAt(_random.Next(0, _flaws.Count))}, ";
                }
            }

            return flaws;
        }
    }
}
